module.exports = {
        "apps": [
                {
                "name": "rasa_web_dev",
               	"script": "/home/srs/projects/knock-knock/article1/rest_api/manage.py",
                "args": ["runserver", "127.0.0.1:8010"],
                "exec_mode": "fork",
                "instances": 1,
                watch: true,
                "wait_ready": true,
                "autorestart": false,
                "max_restarts": 5,
                "exec_interpreter" : "/home/srs/projects/knock-knock/env/bin/python"
                },

	]
}
