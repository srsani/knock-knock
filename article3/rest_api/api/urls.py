from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers

from api import(views
                )


router = routers.DefaultRouter()

urlpatterns = [

    path('', include(router.urls)),
    path('api/hello-world/',
         views.HelloWorld.as_view()),
    path('api/slack-events/',
         views.SlackEvents.as_view()),

]
