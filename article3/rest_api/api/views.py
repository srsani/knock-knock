from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.generics import (GenericAPIView)
from rest_framework.views import APIView

from rest_framework import status

# added slack token to the .env file
from django.conf import settings

from slack import WebClient


# read slack credentials creat an object from WebClient
SLACK_VERIFICATION_TOKEN = getattr(settings, 'SLACK_VERIFICATION_TOKEN', None)
SLACK_USER_OAUTH_ACESS_TOKEN = getattr(
    settings, 'SLACK_USER_OAUTH_ACESS_TOKEN', None)

client = WebClient(token=SLACK_USER_OAUTH_ACESS_TOKEN)


class HelloWorld(GenericAPIView):
    def get(self, request, *args, **kwargs):
        return Response({'hello': 'word'})


class SlackEvents(GenericAPIView):

    def post(self, request, *args, **kwargs):
        slack_message = request.data
        # print(slack_message)
        # 1. varify the slack token
        if slack_message.get('token') != SLACK_VERIFICATION_TOKEN:
            return Response(status=status.HTTP_403_FORBIDDEN)

        # 2. verification challenge
        if slack_message.get('type') == 'url_verification':
            return Response(data=slack_message,
                            status=status.HTTP_200_OK)
        # 3 check if there is an event i the slack message
        if 'event' in slack_message:
            event_message = slack_message.get('event')

            # 4. Ignore the messages sent by the
            bot_id = event_message.get('bot_id')
            if bot_id:
                return Response(status=status.HTTP_200_OK)

            # 5. if the message is sent by the user
            user = event_message.get('user')

            text = event_message.get('text')
            channel = event_message.get('channel')
            bot_text = 'Hey <@{}> :wave:'.format(user)

            # check for greeting phrases in the user messages
            greeting = ['hi', 'hello', 'good morning', 'good afternoon']
            if text.lower() in greeting:
                client.chat_postMessage(channel=channel,
                                        text=bot_text)
                return Response(status=status.HTTP_200_OK)

        return Response(status=status.HTTP_200_OK)
