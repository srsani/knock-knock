from django.db import models


class SlackCommunications(models.Model):

    slack_user_name = models.CharField(blank=True, null=True, max_length=64)

    slack_response_text = models.TextField(blank=True)
    channel = models.TextField(blank=True)

    is_chatbot = models.BooleanField(default=False)
    chatbot_reply = models.JSONField(blank=True, null=True,)
    slack_response = models.JSONField(blank=True, null=True,)

    finished = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.slack_user_name} {self.slack_response_text} SlackCommunications'


class KnockKnock(models.Model):
    name = models.TextField(blank=True)
    punchline = models.TextField(blank=True)

    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'
