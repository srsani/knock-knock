from django.contrib import admin
from django.urls import path, include


admin.site.site_header = "knock-knock"
admin.site.site_title = " Admin Portal"
admin.site.index_title = "Welcome to knock-knock Admin Portal"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('api.urls')),
]
