from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.generics import (GenericAPIView)
from rest_framework.views import APIView


class HelloWorld(GenericAPIView):
    def get(self, request, *args, **kwargs):
        return Response({'hello': 'word'})
